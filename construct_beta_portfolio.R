construct_beta_portfolio <- function(mlr_predict_models, data_mlx) {
  # get only stock_id and beta values (with 0 beta values replaced with NA)
  mlr_predict_models_test <-  mlr_predict_models %>%
    select(-c(n, model, Alpha))
  mlr_predict_models_test[mlr_predict_models_test == 0] <- NA
  
  r1m_ret <- data_mlx[order(data_mlx$monthid),] %>% 
    select(stock_id, R1M_Usd, monthid) %>%
    merge(mlr_predict_models_test, by="stock_id", all = TRUE)
  
  features <- colnames(mlr_predict_models_test)
  features <- features[grepl("_beta", features, fixed = TRUE)]
  
  # for each month get returns of each factor portfolio based on stock's beta value
  #   i.e., factor portfolio = long stocks with high beta, short stocks with low beta
  beta_portfolio_ret = data.frame(monthid = unique(r1m_ret$monthid))
  
  # group stocks by beta values for each factor
  for (feature in features) {
    r1m_ret <- r1m_ret %>% group_by(monthid) %>% mutate(!!sym(feature) := ntile(!!sym(feature), 10))
  }
  
  for (feature in features) {
    decile_df <- r1m_ret %>%
      filter(is.na(!!sym(feature)) == FALSE) %>%
      group_by(monthid, !!sym(feature)) %>%
      summarize(decile_ret := mean(R1M_Usd), .groups="drop") %>%
      ungroup() %>%
      group_by(monthid) %>%
      summarize(f_ret = decile_ret[!!sym(feature) == 1],
                l_ret = decile_ret[!!sym(feature) == 10])
    # decile_df <- decile_df %>%
    #   mutate(paste(feature, "_high") = decile_df$l_ret) %>%
    #   mutate(paste(feature, "_low") = decile_df$f_ret)
    decile_df[paste(feature, "_high")] = decile_df$l_ret
    decile_df[paste(feature, "_low")] = decile_df$f_ret
    decile_df <- decile_df %>% select(c(monthid, !!sym(paste(feature, "_high")), !!sym(paste(feature, "_low"))))
    beta_portfolio_ret <- merge(beta_portfolio_ret, decile_df, by="monthid")
  }
  
  return(beta_portfolio_ret)
}