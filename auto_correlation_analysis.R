library(tseries)
library(dplyr)
library(tidyverse)

factor_AR <- factor_ret
factor_AR <- factor_AR[order(factor_AR$monthid),]
factor_AR <- select(factor_AR, -monthid)

columns <- colnames(factor_AR)

for (colname in columns) {
  new_col <- paste(colname, "_shifted", sep="")
  factor_AR[[new_col]] <- lag(factor_AR[[colname]])
}

factor_AR <- factor_AR %>%
  filter(row_number() %% 2 == 1) %>%
  na.omit()

factor_AR_results <- data.frame(name=NA, AR1 = NA, pval = NA)

for (colname in columns) {
  new_col <- paste(colname, "_shifted", sep="")
  cor <- cor.test(factor_AR[[colname]],factor_AR[[new_col]])
  factor_AR_results <- factor_AR_results %>%
    add_row(name = colname, AR1 = cor$estimate, pval = cor$p.value)
}

factor_AR_results <- filter(factor_AR_results, pval <= 0.05)
#factor_AR_results <- filter(factor_AR_results, AR1 > 0)